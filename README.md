# setup

```
pipenv install
```

# start/restart backend

```
cd backend
./bootstrap &
```

# stop backend

```
pkill flask
```

# start frontend

```
# in a new terminal window
cd frontend
./bootstrap
```

# start response listener for troubleshooting

```
pipenv run python slurp-commands-queue.py
```

# watch queue list

```
sudo watch 'rabbitmqctl list_queues'
```
