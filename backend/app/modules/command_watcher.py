import json
import pika
import os

from threading import Thread
from app import combat
from app import creatures
from app import rooms
from app.publisher import publish

# phase 2: command is pulled from command queue
def consume_command(ch, method, properties, body):
    command = body.decode("utf-8")

    if command.split(" ")[0] == "attack":
        try:
            target = command.split(" ")[1]
            combat.attack_creature(target)
        except IndexError:
            publish("attack what?", "responses")
    elif command == "look":
        rooms.get_current()
        creatures.list_creatures()
    else:
        publish("invalid command <<{}>>".format(command), "responses")


# this block is repeated for each new queue; parameterize?
# this sets up the watcher for commands
amqp_url = os.environ['AMQP_URL']
print ("ampq_url is {}".format(ampq_url))
connection = pika.BlockingConnection(pika.URLParameters(amqp_url))
channel = connection.channel()
channel.queue_declare(queue='commands')
channel.basic_qos(prefetch_count=1)
channel.basic_consume(consume_command,
                      queue='commands',
                      no_ack=True)
thread = Thread(target=channel.start_consuming)
thread.start()
thread.join(0)
