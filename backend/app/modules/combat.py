#!venv/bin/python

import json
import pika

from modules.creatures import creatures
from modules.publisher import publish

def attack_creature(target):
    try:
        creature_id = [d for d in creatures if d['name'] == target][0]['id']
    except IndexError:
        publish("there is no {} here".format(target), "responses")
        return

    data = json.dumps({"creature_id": creature_id, "action": "damage", "amount": 2})
    publish(data, "creature_state")
