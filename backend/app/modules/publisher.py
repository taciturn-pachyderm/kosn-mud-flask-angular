import json
import pika
import os

def publish(data, target_queue):
    amqp_url = os.environ['AMQP_URL']
    print("publishing to {}".format(target_queue))
    print ("ampq_url is {}".format(amqp_url))
    connection = pika.BlockingConnection(pika.URLParameters(amqp_url))
    channel = connection.channel()
    channel.queue_declare(queue=target_queue)
    channel.basic_publish(exchange='',
                      routing_key=target_queue,
                      body=data)
    connection.close()
