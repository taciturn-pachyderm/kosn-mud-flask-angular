#!venv/bin/python

from app.publisher import publish

rooms = [
    {'name': 'courtyard', 'current': True},
    {'name': 'tower', 'current': False}
]

def get_current():
    global rooms
    # what happens if we're not in any room?
    try:
        current = [d['name'] for d in rooms if d['current'] == True][0]
        publish("you are in a {}".format(current), "responses")
    except IndexError:
        publish("you are nowhere... nothing makes sense anymore...", "responses")
        raise "no room is active, how could this happen?"
