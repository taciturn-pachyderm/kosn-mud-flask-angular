#!venv/bin/python

import json
import pika
import os
from threading import Thread

from app.publisher import publish

creatures = [
    {'id': 1, 'name': 'platypus', 'hp': '10'},
    {'id': 2, 'name': 'aardvark', 'hp': '10'}
]

def join(seq, sep=', '):
    return sep.join(str(i) for i in seq)

def list_creatures():
    creature_names = [d['name'] for d in creatures]
    names_to_string = join('%s' % entry for entry in creature_names)
    publish("there are creatures here: {}".format(names_to_string), "responses")

def process_state_change(ch, method, properties, body):
    instruction = json.loads(body.decode("utf-8"))
    creature_id = instruction["creature_id"]

    creature = {}
    try:
        creature = [d for d in creatures if d['id'] == creature_id][0]
    except IndexError:
        raise "something went wrong; id <<{}>> " +\
            "does not exist in creatures: {}".format(creature_id, creatures)

    message = ""
    if instruction["action"] == 'damage':
        hp = int(creature["hp"])
        creature["hp"] = str(hp - instruction["amount"])
        message = "attacked {} for 1 damage; {} hit points are now {}"\
            .format(creature["name"], creature["name"], creature["hp"])
    publish(message, "responses")


# this block is repeated for each new queue; parameterize?
# this sets up the watcher for creature_state operations
amqp_url = os.environ['AMQP_URL']
print ("ampq_url is {}".format(ampq_url))
connection = pika.BlockingConnection(pika.URLParameters(amqp_url))
channel = connection.channel()
channel.queue_declare(queue='creature_state')
channel.basic_qos(prefetch_count=1)
channel.basic_consume(process_state_change,
                      queue='creature_state',
                      no_ack=True)
thread = Thread(target=channel.start_consuming)
thread.start()
thread.join(0)
