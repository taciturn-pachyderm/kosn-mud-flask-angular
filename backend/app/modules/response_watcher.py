import pika
import json
import os
from threading import Thread

things = []

# phase 3 consume response messages
def consume_response(ch, method, properties, body):
    things.insert(0, {"response": body.decode("utf-8")})


# this block is repeated for each new queue; parameterize?
# this sets up the watcher for responses
amqp_url = os.environ['AMQP_URL']
print("opening connection to {}".format(amqp_url))
connection = pika.BlockingConnection(pika.URLParameters(amqp_url))
channel = connection.channel()
channel.queue_declare(queue='responses')
channel.basic_qos(prefetch_count=1)
channel.basic_consume(consume_response,
                      queue='responses',
                      no_ack=True)
thread = Thread(target=channel.start_consuming)
thread.start()
thread.join(0)

def get_things():
    return things
