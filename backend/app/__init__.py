from flask_cors import CORS
from flask import Flask, jsonify, request
from datetime import datetime
from threading import Thread

import json
import pika
import time

from app.modules.response_watcher import get_things
from app.modules.publisher import publish


def make_app():
    app = Flask(__name__)
    CORS(app)

    @app.route('/command', methods=['POST'])
    def add_command():
        print("Request json is {}".format(request.get_json()))
        command = request.get_json()["command"]
        publish(command, "commands")
        return jsonify("command <<{}>> published".format(command))

    @app.route('/responses')
    def get_responses():
        to_return = get_things()
        return json.dumps(to_return)

    @app.route('/status')
    def status():
        return "you made it, Jim"

    return app

if __name__ == '__main__':
    app = make_app()
    app.run(port=8000)
