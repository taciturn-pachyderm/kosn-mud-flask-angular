#!/bin/bash

if pgrep flask; then pkill flask; fi
export FLASK_APP=./app/main.py
source $(pipenv --venv)/bin/activate
flask run -h 0.0.0.0
